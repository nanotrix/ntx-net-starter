# Setup
1. Install git, [VS2017+](https://www.visualstudio.com/downloads/)
1. Checkout this project:
    ```
    git clone https://gitlab.com/nanotrix/ntx-net-starter.git
    cd ntx-net-starter
    ```
1. Checkout ntx project, use -b switch to choose branch version
    ```
    git clone -b 1.0.1 https://gitlab.com/nanotrix/ntx.git
    ```
3. Open solution, restore nuget packages and build

# Samples
1. auth => how to get/revoke access/refresh token (net461)
2. v2t => how to do transcription (net461)
3. httpbridge -> simple http transciption service (netcore2.0), try with
    * Transcribe file via POST form
    ```
    curl  -F "input=files/basetext.mp3" -F "tags=lang:cz type:broadcast" http://localhost:8080/task/run
    ```
    * Transcribe live stream 
    ```
    curl  -F "input=http://icecast8.play.cz/cro1-128.mp3" -F audioformat=auto:8196 -F "tags=lang:cz type:broadcast" http://localhost:8080/task/run
    ``` 

## See ntx client project on [gitlab](https://gitlab.com/nanotrix/ntx.git) for more info ...

