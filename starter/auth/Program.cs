﻿using ntx.api.auth;
using ntx.api.io;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace refreshtoken
{
    class Program
    {
        static void Main(string[] args)
        {
           var asyncTask = Task.Run(async () =>
              {
                  
                  //Exchange your credentials with  refresh token, do this only once, refresh token is valid forever 
                  bool getRefreshToken = true; // set to false to get just time-limited access token
                  Console.WriteLine("To get refresh token: login to https://xxx.nanogrid.cloud and enter following:");
                  Console.WriteLine("Enter username:");
                  var UserName = Console.ReadLine();
                  Console.WriteLine("Enter clientId:");
                  var ClientId = Console.ReadLine();
                  Console.WriteLine("Enter audience:");
                  var Audience = Console.ReadLine();
                  Console.WriteLine("Enter issuer:");
                  var Issuer = Console.ReadLine();
                  Console.WriteLine("Enter password:");
                  var Password = ntx.api.util.Utils.ReadLineMasked();
                  Console.WriteLine();
                  Console.WriteLine("Your refresh token (paste it into https://jwt.io/) to see content:");

                  var outputStream = LazyStream.Output("-", "text/plain");
                  try
                  {
                      var token = await Authorization.LoginWithPassword(UserName, Password, ClientId, Issuer, Audience, getRefreshToken);
                      using (var w = new StreamWriter(outputStream, new UTF8Encoding(false)))
                      {
                          await w.WriteLineAsync(token);
                      }
                      //refresh token is revoked via api, access token is just deleted from disk

                      Console.WriteLine("Press enter to revoke it or Ctrl+C to keep it:");
                      Console.ReadLine();
                      var tokenstream = LazyStream.Input($"string://{token}"); //string as a stream
                      await Authorization.Logout(tokenstream, getRefreshToken);
                  }catch(Exception ex)
                  {
                      Console.WriteLine($"Error: {ex.Message}");
                  }

              });
            asyncTask.Wait();
        }
    }
}
