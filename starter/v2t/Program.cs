﻿using cz.ntx.proto.v2t.engine;
using ntx.api.auth;
using ntx.api.io;
using ntx.api.pipe;
using ntx.api.pipe.events;
using ntx.api.pipe.tasks;
using ntx.api.util;
using System;
using System.Collections.Async;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transcribestream
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.InputEncoding = Encoding.UTF8;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;


            if (System.Environment.GetEnvironmentVariable("NTX_TOKEN")==null)
            {
                Console.WriteLine("set refresh or access token to NTX_TOKEN");
                Console.WriteLine("on win: setx NTX_TOKEN $token");
                Console.WriteLine("on lin: export NTX_TOKEN=$token");
            }
            var LabelOption = "vad+v2t+ppc";
            var TagsOption = new string[] { "lang:cz", "type:broadcast" };
            var InputStreamOption = "http://icecast8.play.cz/cro1-128.mp3";
            var ChannelOption = "downmix";
            var AudioFormatOption = "auto:8192";
            var LexiconUriOption = "none";
            bool jsonOutput = false;
            var asyncTask = Task.Run(async () =>
            {
                
                //***Intialize - global intitalization for user
                //stream with valid token
                var tokenStream = LazyStream.Input("env://NTX_TOKEN");
                //valid access token generator
                var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);
                //Get grpc endpoint from access token
                var endpointUri = new Uri((await accessTokenFactory()).ServiceEndpoint);

                //Create grpc channel, channel can be used for multiple clients of various services
                var channel = endpointUri.ToGRPCChannel();


                //*** do this to setup transcription task 

                //Use task selector to choose one
                var task = await PipeSource.TasksFromLocalStore(accessTokenFactory)
                    .ViaFilterAsync("ntx.v2t.engine.EngineService", LabelOption, TagsOption).FirstAsync();

                //Get factory for producing valid task tokens
                var authorizedTaskFactory = task.ToAuthorizedTaskFactory(accessTokenFactory);

                



                ///***Repeat this for individual trascription sessions for given task

                //Get input lazy stream
                var inputStream = LazyStream.Input(InputStreamOption);

                //Stream where to print output, dash means stdout
                var outputResolver = LazyStream.Output("-", "application/json");


                //Fill config for transcription
                var engineContext = new EngineContext()
                    .ParseCmd(LabelOption, ChannelOption);

                engineContext.AudioFormat = new AudioFormat()
                    .ParseCmd(AudioFormatOption); ;

                //Add lexicon if any
                await engineContext.AddLexiconFromUri(LexiconUriOption);

                //Create grpc  client on a channel
                var client = new EngineService.EngineServiceClient(channel);

                //stop transcription after 10 minutes;
                var tokenSource = new System.Threading.CancellationTokenSource(600000);

                //Create pipe 
                var pipe = PipeSource.EventsFromBinaryStream(inputStream, 4096, tokenSource.Token) //source of events
                           .ViaV2TStreaming(client, engineContext, authorizedTaskFactory); //translate via v2t engine


                if (jsonOutput)
                {

                    //Create sink from output stream
                    var sink = PipeSink.ProtoJsonStream<Events>(
                         outputResolver
                        );

                    //Run pipe
                    await pipe.RunWithSink(sink);
                }else
                {
                    //Create sink from output stream
                    var sink = PipeSink.StringChunkStream(
                         outputResolver
                        );

                    //or use eg this to see text only
                    await pipe.ViaLookAheadFilter() //remove potential lookahead
                    .ToCustomFormat() //
                    .RunWithSink(sink);
                }
                
                



            });
            asyncTask.Wait();
        }
    }
    static class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<string> ToCustomFormat(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource, bool withNoises=false)
        {
            return new AsyncEnumerable<string>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var events = source.Current.Events_;
                    var ts = events.FirstOrDefault(x => x.BodyCase == Event.BodyOneofCase.Timestamp && x.Timestamp.ValueCase== Event.Types.Timestamp.ValueOneofCase.Timestamp_);
                    if(ts!=null)
                    {
                        var t = TimeSpan.FromTicks((long)ts.Timestamp.GetValue());
                        await yield.ReturnAsync($"time={t}\n");
                    }
                    string text = "";
                    if (withNoises)
                    {
                        text = string.Join("", events.Where(x => x.BodyCase == Event.BodyOneofCase.Label).Select(x => x.Label.GetValue()));
                    }
                    else
                    {
                        text = string.Join("", events.Where(x => x.BodyCase == Event.BodyOneofCase.Label && x.Label.LabelCase != Event.Types.Label.LabelOneofCase.Noise).Select(x => x.Label.GetValue()));
                    }
                    if (text.Length == 0)
                        continue;
                    var line = $"{text}";
                    await yield.ReturnAsync(line + "\n");

                }
            });
        }
    }
}
