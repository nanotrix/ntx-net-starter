﻿using cz.ntx.proto.v2t.engine;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using ntx.api.auth;
using ntx.api.io;
using ntx.api.pipe;
using ntx.api.pipe.events;
using ntx.api.pipe.tasks;
using ntx.api.util;

using System;
using System.Collections.Async;

namespace httpbridge
{
    class Program
    {
        
        static void Main(string[] args)
        {

            if (System.Environment.GetEnvironmentVariable("NTX_TOKEN") == null)
            {
                Console.WriteLine("set refresh or access token to NTX_TOKEN");
                Console.WriteLine("on win: setx NTX_TOKEN $token");
                Console.WriteLine("on lin: export NTX_TOKEN=$token");
            }
            new Bridge().Run().Wait();
        }
    }

    class Bridge
    {
        private Func<System.Threading.Tasks.Task<Authorization.TaskAccessToken>> accessTokenFactory;
        private Grpc.Core.Channel grpcChannel;

        public async System.Threading.Tasks.Task Run()
        {
            var bind = "http://127.0.0.1:8080";
            var baseAddress = "task";

            //valid access token generator
            accessTokenFactory = Authorization.AccesTokenFactory(
                LazyStream.Input("env://NTX_TOKEN")
                );

            //sevice Uri
            var endpointUri = new Uri((await accessTokenFactory()).ServiceEndpoint);
            //Create grpc channel, channel can be used for multiple clients of various services
            grpcChannel = endpointUri.ToGRPCChannel();


            var host = new WebHostBuilder()
               .UseKestrel()
               .UseUrls(bind)
               //.UseLoggerFactory(Logging.LoggerFactory)
               .ConfigureServices((services) =>
               {
                   services.AddRouting();

               }).Configure(app =>
               {

                   //Configure router ... see https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing
                   var builder = new RouteBuilder(app, new RouteHandler(ctx => NotFound(ctx)));
                   ConfigureRouter(baseAddress, builder);
                   var routes = builder.Build();
                   app.UseRouter(routes);

               })
               .Build();
            await host.RunAsync();
        }
        private void ConfigureRouter(string baseAddress, RouteBuilder builder)
        {
            builder.MapPost(baseAddress + $"/run", EngineServiceRun);
        }
        private async System.Threading.Tasks.Task NotFound(HttpContext ctx)
        {
            ctx.Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            await ctx.Response.WriteAsync("Not found");
        }
        private async System.Threading.Tasks.Task EngineServiceRun(HttpContext ctx)
        {
            var form = await ctx.Request.ReadFormAsync();

            var labelOption = form.ContainsKey("label") ? form["label"].ToString() : "vad+v2t+ppc";
            var tagsOption = form.ContainsKey("tags") ? form["tags"].ToString() : "";
            var audioOption = form.ContainsKey("audioformat") ? form["audioformat"].ToString() : "auto:0";
            var channelOption = form.ContainsKey("channel") ? form["channel"].ToString() : "downmix";
            var chunkSize = 4096;
            var lexiconOption = form.ContainsKey("lexicon") ? form["lexicon"].ToString() : "none";
            var inputOption = form.ContainsKey("input") ? form["input"].ToString() : "-";

            ;
            string initError = "Get task from store failed";
            try
            {
                //Use task selector to choose one

                var task = await PipeSource.TasksFromLocalStore(accessTokenFactory, ctx.RequestAborted)
                    .ViaFilterAsync("ntx.v2t.", labelOption, tagsOption.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).FirstAsync().ViaInterceptor(
                        x =>
                        {
                            Console.Error.WriteLine($"Using task: {x.Service} -l {x.Profiles[0].Labels[0]} -t {string.Join(" -t ", x.Tags)}");
                            return x;
                        }
                    );
                initError = "Authorization of task failed";
                var authorizedTaskFactory = task.ToAuthorizedTaskFactory(accessTokenFactory);
                initError = "Invalid input";
                if (inputOption == "-")
                    throw new Exception($"stdin not allowed");

                var inputStreamResolver = LazyStream.Input(inputOption, ctx.RequestAborted);
                initError = "Invalid option";
                //Fill v2t config
                var engineContext = new EngineContext()
                    .ParseCmd(labelOption, channelOption);
                engineContext.AudioFormat = new AudioFormat()
                    .ParseCmd(audioOption); ;

                initError = "Lexicon load failed";
                //Add lexicon if any
                await engineContext.AddLexiconFromUri(lexiconOption);
                //Select reader
                bool textInput = engineContext.ConfigCase == EngineContext.ConfigOneofCase.Ppc;
                initError = "Create pipesource failed";
                //Create pipe source
                var eventsSource = textInput ?
                    PipeSource.FromProtoJsonStream<Events>(inputStreamResolver, ctx.RequestAborted)
                    .ViaEventsLimiterByCount(10)
                    : PipeSource.EventsFromBinaryStream(inputStreamResolver, (int)chunkSize, ctx.RequestAborted);

                initError = "Create grpc client failed";
                //Create client on channel
                var client = new EngineService.EngineServiceClient(grpcChannel);

                try
                {
                    //Create sink from output stream
                    var sink = PipeSink.ProtoJsonStream<EngineStream>(
                         ctx.Response.Body
                        );
                    //Run pipe
                    await eventsSource.ViaV2TStreaming(client, engineContext, authorizedTaskFactory).ToEngineStream().RunWithSink(sink);

                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine($"Failed with: {ex.Message}");
                    return;
                }
            }
            catch (Exception ex)
            {

                Console.Error.WriteLine($"{initError}: {ex}");
                ctx.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                await ctx.Response.WriteAsync($"{initError}: {ex.Message}");
            }




        }
    }


}

