﻿using cz.ntx.proto.v2t.engine;
using System;
using System.Collections.Async;
using System.Text;

namespace httpbridge
{
    public static partial class Extensions
    {
        public static IAsyncEnumerable<EngineStream> ToEngineStream(this IAsyncEnumerable<Events> eventsSource)
        {
            return new AsyncEnumerable<EngineStream>(async yield =>
            {
                await yield.ReturnAsync(new EngineStream { Start = new EngineContextStart { } });
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                
                while (true)
                {
                    try
                    {
                        if (!await source.MoveNextAsync())
                        {
                            await yield.ReturnAsync(new EngineStream { End = new EngineContextEnd() });
                            break;
                        }
                        await yield.ReturnAsync(new EngineStream { Push = new EventsPush { Events = source.Current } });
                    }
                    catch(Exception ex)
                    {
                        await yield.ReturnAsync(new EngineStream { End = new EngineContextEnd() { Error = ex.Message } });
                        break;
                    }
                }
            }
           );
        }
    }
}
